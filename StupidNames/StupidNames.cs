﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StupidNames
{
    class StupidNames
    {
        private static List<String> m_names = new List<string>();
        private static Char[] m_buchstaben = new Char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

        private static void add(String _newName)
        {
            if(!m_names.Contains(_newName.ToLower()))
            {
                m_names.Add(_newName.ToLower());
            }
        }

        private static Char getNextChar(Char _first)
        {
            bool _r = false;

            foreach (Char _c in m_buchstaben)
            {
                if (_r == true)
                    return _c;

                if (_first == _c)
                    _r = true;
            }

            return ' ';
        }

        static void Main(string[] args)
        {
            if(File.Exists("names.txt"))
            {
                String[] _namesFromFile = File.ReadAllLines("names.txt");

                foreach (String _name in _namesFromFile)
                {
                    add(_name);
                }

                Console.WriteLine("Namen: " + m_names.Count);
            }

            foreach(String _name in m_names)
            {
                char[] _einzeln = _name.ToLower().ToCharArray();
                String _neuerName = "";

                for (int i = 0; i < _einzeln.Length; i++)
                {
                    foreach (Char _c in m_buchstaben)
                    {
                        if (_einzeln[i] == _c)
                        {
                            _neuerName += getNextChar(_c);
                        }
                    }
                }

                if (m_names.Contains(_neuerName))
                {
                    Console.WriteLine("Gefunden: "+ _name + " == " + _neuerName);
                }
            }

            Console.WriteLine("----- Fertig -----");
            Console.ReadLine();
        }
    }
}
